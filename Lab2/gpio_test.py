#!/usr/bin/python
import time
from gpio import IoTGPIO

pigpio = IoTGPIO()
while True:
	switch = pigpio.read_switch()
	print('\n =======')
	print('\n Switch: {0}'.format(switch))
	pigpio.set_led(1, True)
	pigpio.set_led(2, True)
	print('\n LED1: {0}'.format(pigpio.get_led(1)))
	print('\n LED2: {0}'.format(pigpio.get_led(2)))
	time.sleep(1.0)
	switch = pigpio.read_switch()
	print('\n =======')
	print('\n Switch: {0}'.format(switch))
	pigpio.set_led(1, False)
	pigpio.set_led(2, False)
	print('\n LED1: {0}'.format(pigpio.get_led(1)))
	print('\n LED2: {0}'.format(pigpio.get_led(2)))
	time.sleep(1.0)

